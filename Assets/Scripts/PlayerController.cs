using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private Camera cam;

    public float speed = 5;
    public float downwardPush = 10;

    private Rigidbody rb;
    private UIController UI;

    private Memory lastMemory;
    private Memory currentMemory;

    bool lookingAtOldThing = false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        UI = UIController.Instance;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void FixedUpdate()
    {
        if (!UIController.Instance.readingMemory)
        {
            rb.AddRelativeForce(speed * new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
            Ray ray = new Ray(transform.position + Vector3.up * 0.2f, Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 5, LayerMask.GetMask("House")))
            {
                Debug.DrawLine(transform.position, hit.point, Color.blue);
                if (hit.distance > 0.4f)
                {
                    rb.AddRelativeForce(Vector3.down * downwardPush);
                }
            }
        }
    }

    private void Update()
    {
        transform.forward = Vector3.ProjectOnPlane(cam.transform.forward, Vector3.up).normalized;

        RaycastHit hit;
        Ray ray = new Ray(cam.transform.position, cam.transform.forward);

        if (Physics.Raycast(ray, out hit, 3) && hit.collider.CompareTag("Memory") && !UI.readingMemory)
        {

            Memory temp = hit.collider.GetComponent<Memory>();
            if (lookingAtOldThing)
            {
                if (temp != lastMemory)
                {
                    UI.LookAtObject(temp);
                    lookingAtOldThing = false;
                }
                UI.LookAtObject(temp);
                return;
            }
            if (currentMemory == null)
            {
                currentMemory = temp;
                currentMemory.BeginInteract();
                UI.LookAtObject(currentMemory);
            }
            else if (temp != currentMemory)
            {
                lastMemory = currentMemory;
                currentMemory.StopInteracting();
                currentMemory = temp;
                currentMemory.BeginInteract();
                UI.LookAtObject(currentMemory);
            }

            if (Input.GetMouseButtonDown(0))
            {
                currentMemory.Interact();
                UI.LookAtObject();
                lookingAtOldThing = true;
            }
        }
        else
        {
            if (lookingAtOldThing && !UI.readingMemory)
            {
                lookingAtOldThing = false;
                UI.LookAtObject();
            }
            if (currentMemory != null)
            {
                lastMemory = currentMemory;
                currentMemory.StopInteracting();
                currentMemory = null;
            }
            UI.LookAtObject();
        }
    }
}
