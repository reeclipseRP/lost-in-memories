using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Memory : MonoBehaviour, IDescriptable
{
    [Tooltip("When player hovers over the memory")]
    public UnityEvent OnBeginHovering;
    [Tooltip("When player stops hovering over the memory")]
    public UnityEvent OnStopHovering;

    [Tooltip("When player interact with a memory (reads it)")]
    public UnityEvent OnInteract;
    [Tooltip("When player stops reading the memory")]
    public UnityEvent OnInteractFinish;

    public UnityEvent OnInteractForTheFirstTime;
    public UnityEvent OnInteractForTheFirstTimeFinish;

    public float badOrGoodObject;

    [TextArea]
    public string story;

    [ShowAssetPreview]
    public Sprite image;

    static bool curentlyActive = false;
    
    [HideInInspector]
    public bool hasBeenInteracted = false;

    public string Description()
    {
        return story;
    }

    public string Name()
    {
        return gameObject.name;
    }

    public void Interact()
    {
        if(curentlyActive)
        {
            return;
        }
        curentlyActive = true;
        UIController.Instance.DisplayMemory(this);
        OnInteract?.Invoke();

        // Is called when interacting with memory for the first time
        if(!hasBeenInteracted)
        {
            OnInteractForTheFirstTime?.Invoke();
        }

        Debug.Log("Interact");
    }
    public void MemoryRead()
    {
        curentlyActive = false;
        OnInteractFinish?.Invoke();
        Debug.Log("End interact");

        // Is called when finishing interacting with the memory for the first time
        if(!hasBeenInteracted)
        {
            OnInteractForTheFirstTimeFinish?.Invoke();
            hasBeenInteracted = true;
            LevelManager.Instance.CheckCompletion();
        }
    }
    public void BeginInteract() // Gets called when object is getting hovered
    {
        OnBeginHovering?.Invoke();
        Debug.Log("Start looking");
    }
    /// <summary>
    /// Gets called when object is stoped getting hoverd
    /// </summary>
    public void StopInteracting()
    {
        OnStopHovering?.Invoke();
        Debug.Log("Stop looking");
    }
}
