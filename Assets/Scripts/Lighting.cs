using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

[RequireComponent(typeof(Animator))]
public class Lighting : MonoBehaviour
{
    private Animator an;
    private void Awake()
    {
        an = GetComponent<Animator>();
    }
    private void Reset()
    {
        an = GetComponent<Animator>();
    }

    [Button]
    public void Strike()
    {
        gameObject.SetActive(true);
        an.Play("Thunder");
        DOVirtual.DelayedCall(1f,() => gameObject.SetActive(false));
        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, Random.Range(0,360), transform.eulerAngles.z);
    }
}
