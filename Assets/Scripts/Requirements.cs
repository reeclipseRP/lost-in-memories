using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Requirements : MonoBehaviour
{
    public UnityEvent OnFinished;

    [SerializeField] private int requiredForCompletion;
    private int current;

    private bool initialized = false;

    private void Initialize()
    {
        current = requiredForCompletion;
        initialized = true;
    }
    public void CompleteStep()
    {
        if (!initialized) Initialize();
        current--;
        if(current <= 0)
        {
            Finished();
        }
    }
    private void Finished()
    {
        OnFinished?.Invoke();
        Destroy(this);
    }
}
