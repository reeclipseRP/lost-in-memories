using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class FModAmbienceControl : MonoBehaviour
{
    public static FModAmbienceControl instance;

    public string FModEventNameCurrentLevel;
    public string FModEventNameTotalMemoriesCollected;
    public string FModEventNameCurrentMemoriesCollected;

    [SerializeField] private TextMeshProUGUI progressText;
    
    FMODUnity.StudioEventEmitter emitter;

    //the current level of build settings
    public int currentLevel = 0;

    //the sum of collected good/bad memory objects.
    //float emotionLevel = 0;

    //how many memory objects are collected
    public int totalMemoriesCollected = 0;

    //how many memory objects are collected
    public int currentLevelMemoriesCollected = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        emitter = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    void Start()
    {
        //progressText.SetText($"Memories 0/{LevelManager.Instance.RequiredMemories.Count}");
    }


    void FixedUpdate()
    {
        UpdateAmbient();
    }

    void UpdateAmbient()
    {
        CalculateParameters();

        UpdateEvent(FModEventNameCurrentLevel, currentLevel);
        UpdateEvent(FModEventNameTotalMemoriesCollected, totalMemoriesCollected);
        UpdateEvent(FModEventNameCurrentMemoriesCollected, currentLevelMemoriesCollected);
    }

    void UpdateEvent(string EventName, float value)
    {
        emitter.SetParameter(EventName, value);
    }
    
    void UpdateEvent(string EventName, int value)
    {
        emitter.SetParameter(EventName, value);
    }

    public void AddTotalCollected()
    {
        totalMemoriesCollected++;
    }

    void CalculateParameters()
    {
        int collected = 0;
        for(int i = 0; i < LevelManager.Instance.RequiredMemories.Count; i++)
        {
            if(LevelManager.Instance.RequiredMemories[i].hasBeenInteracted)
            {
                collected++;
            }
        }
        //progressText.SetText($"Memories {collected}/{LevelManager.Instance.RequiredMemories.Count}");
        UIController.Instance.Progress.SetText($"Memories {collected}/{LevelManager.Instance.RequiredMemories.Count}");

        currentLevelMemoriesCollected = collected;

        currentLevel = SceneManager.GetActiveScene().buildIndex;
    }
}
