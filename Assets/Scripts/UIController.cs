using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UIController : MonoBehaviour
{
    public static UIController Instance;
    private void Awake()
    {
        Instance = this;
    }

    [SerializeField] private TextMeshProUGUI ObjectNameText;
    [SerializeField] private Image Cursor;

    [SerializeField] private CanvasGroup cg;
    [SerializeField] private TextMeshProUGUI MemoryDescriptionText;
    [SerializeField] private Image MemoryImage;

    public TextMeshProUGUI Progress;

    [SerializeField] private Image FadeOverlayImage;

    private bool displayed = true;
    private bool transitioning = false;
    public bool readingMemory = false;

    private Memory currentMemory;

    [SerializeField] private float fadeDuration;


    private void Start()
    {
        LookAtObject();
        Color tmp = FadeOverlayImage.color;
        tmp.a = 1;
        FadeOverlayImage.color = tmp;
        FadeIn();
    }
    public void LookAtObject(IDescriptable thing = null)
    {
        if (readingMemory)
        {
            ObjectNameText.alpha = 0;
            Cursor.gameObject.SetActive(false);
        }
        else
        {
            bool isNull = (thing == null);
            if (!isNull)
            {
                displayed = true;
                ObjectNameText.SetText(thing.Name());
                FirstLook();
            }
            else if (isNull && displayed)
            {
                displayed = false;
                LastLook();
            }
            Cursor.gameObject.SetActive(isNull && !transitioning);
        }
    }

    private void FirstLook()
    {
        Cursor.DOFade(0, 0.1f);
        ObjectNameText.alpha = 0;
        ObjectNameText.DOFade(1, 0.5f);
    }
    private void LastLook()
    {
        transitioning = true;
        ObjectNameText.DOFade(0, 0.5f).OnComplete(() =>
        {
            transitioning = false;
            Cursor.DOFade(1, 0.2f);
        });
    }
    public void DisplayMemory(Memory memory)
    {
        cg.gameObject.SetActive(true);
        transitioning = true;
        currentMemory = memory;
        readingMemory = true;
        cg.alpha = 0;
        cg.DOFade(1, 2f).OnComplete(() =>
        {
            transitioning = false;
        });

        MemoryImage.gameObject.SetActive(memory.image);
        MemoryImage.sprite = memory.image;
        MemoryDescriptionText.SetText(memory.story);
    }
    public void MemoryRead()
    {
        if (readingMemory)
        {
            transitioning = true;
            cg.DOFade(0, 1f).OnComplete(() =>
            {
                currentMemory.MemoryRead();
                cg.gameObject.SetActive(false);
                readingMemory = false;
                transitioning = false;
            });
        }
    }

    private void Update()
    {
        if (readingMemory && Input.GetMouseButtonDown(0) && !transitioning)
        {
            MemoryRead();
        }
    }

    public void FadeIn(Action onComplete = null)
    {
        FadeOverlayImage.DOFade(0, fadeDuration).OnComplete(() => onComplete?.Invoke());
    }
    public void FadeOut(Action onComplete = null)
    {
        FadeOverlayImage.DOFade(1, fadeDuration).OnComplete(() => onComplete?.Invoke());
    }
}
