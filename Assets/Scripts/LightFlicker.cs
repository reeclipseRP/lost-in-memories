using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightFlicker : MonoBehaviour
{
    private Light lamp;
    private void Awake()
    {
        lamp = GetComponent<Light>();
    }
    private void Start()
    {
        Flicker(lamp);
    }

    public static void Flicker(Light light)
    {
        light.DOKill();
        DOTween.Sequence()
            .Append(light.DOIntensity(Random.value, Random.Range(0.01f, 0.2f)))
            .AppendInterval(Random.Range(0.01f, 0.04f)).OnComplete(() => Flicker(light));
    }
}
