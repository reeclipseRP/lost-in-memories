using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public string nextLevelName;
    public static LevelManager Instance;
    private void Awake()
    {
        Instance = this;
    }

    public List<Memory> RequiredMemories = new List<Memory>();

    public void CheckCompletion()
    {
        if(FModAmbienceControl.instance)
        {
            FModAmbienceControl.instance.AddTotalCollected();
        }
        Debug.Log("Check completion");
        if(RequiredMemories.TrueForAll(memory => memory.hasBeenInteracted))
        {
            LevelFinish();
        }
    }
    private void LevelFinish()
    {
        if(nextLevelName == "Quit")
        {
            Application.Quit();
        }
        else
        {
            UIController.Instance.FadeOut(() => SceneManager.LoadScene(nextLevelName));
        }
        
    }


}
