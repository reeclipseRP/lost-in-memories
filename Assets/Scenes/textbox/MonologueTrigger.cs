using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonologueTrigger : MonoBehaviour
{
    public Monologue monologue;

    public void TriggerMonologue()
    {
        FindObjectOfType<MonologueManager>().StartMonologue(monologue);
    }
}
