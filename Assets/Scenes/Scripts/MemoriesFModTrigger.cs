using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EventTrigger
{
    public string FModParameterName;
    public float value;
}

public class MemoriesFModTrigger : MonoBehaviour
{
    public enum Events
    { 
        OnBeginHovering,
        OnStopHovering,
        OnInteract,
        OnInteractFinish,
        OnInteractForTheFirstTime,
        OnInteractForTheFirstTimeFinish
    };

    public Events PlayEvent;
    public Events StopEvent;

    public List<EventTrigger> triggers = new List<EventTrigger>();

    FMODUnity.StudioEventEmitter emitter;
    Memory memory;


    void Start()
    {
        emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        memory = GetComponent<Memory>();

        switch(PlayEvent)
        {
            case Events.OnBeginHovering:
                memory.OnBeginHovering.AddListener(PlayFModEvent);
                break;
            case Events.OnStopHovering:
                memory.OnStopHovering.AddListener(PlayFModEvent);
                break;
            case Events.OnInteract:
                memory.OnInteract.AddListener(PlayFModEvent);
                break;
            case Events.OnInteractFinish:
                memory.OnInteractFinish.AddListener(PlayFModEvent);
                break;
            case Events.OnInteractForTheFirstTime:
                memory.OnInteractForTheFirstTime.AddListener(PlayFModEvent);
                break;
            case Events.OnInteractForTheFirstTimeFinish:
                memory.OnInteractForTheFirstTimeFinish.AddListener(PlayFModEvent);
                break;
        }

        switch (StopEvent)
        {
            case Events.OnBeginHovering:
                memory.OnBeginHovering.AddListener(StopFModEvent);
                break;
            case Events.OnStopHovering:
                memory.OnStopHovering.AddListener(StopFModEvent);
                break;
            case Events.OnInteract:
                memory.OnInteract.AddListener(StopFModEvent);
                break;
            case Events.OnInteractFinish:
                memory.OnInteractFinish.AddListener(StopFModEvent);
                break;
            case Events.OnInteractForTheFirstTime:
                memory.OnInteractForTheFirstTime.AddListener(StopFModEvent);
                break;
            case Events.OnInteractForTheFirstTimeFinish:
                memory.OnInteractForTheFirstTimeFinish.AddListener(StopFModEvent);
                break;
        }
    }

    private void Update()
    {
        UpdateEvents();
    }

    public void PlayFModEvent()
    {
        UpdateEvents();

        emitter.Play();
    }

    public void StopFModEvent()
    {
        emitter.Stop();
    }

    void UpdateEvents()
    {
        if (emitter.IsPlaying())
        {
            for (int i = 0; i < triggers.Count; i++)
            {
                UpdateEvent(triggers[i].FModParameterName, triggers[i].value);
            }
        }
    }

    public void UpdateEvent(string EventName, float value = 0)
    {
        emitter.SetParameter(EventName, value);
    }
}
