using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TestFMod : MonoBehaviour
{
    public float stationFloat;
    public string EventName = "";

    void Update()
    {
        MyEventWithInt(stationFloat);
    }


    public void MyEventWithInt(float windStrength)
    {
        var emitter = GetComponent<FMODUnity.StudioEventEmitter>();
        emitter.SetParameter(EventName, windStrength);
    }
}
